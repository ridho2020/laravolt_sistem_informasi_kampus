<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dosen;
use DB;

class DosenController extends Controller
{
     public function index()
    {
        $dosens = Dosen::latest()->paginate(10);
        // dd($dosens);
        return view('dosen.index', compact('dosens'));
    }

    public function show($id)
	{
		// get the nerd
		$dosens = Dosen::with('riwayatPendidikan')->find($id);
        //  dd($dosens);
		// show the view and pass the nerd to it
		return view('dosen.show', compact('dosens'));
	}

    public function create()
    {
        return view('dosen.create');
    }

    public function store(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            'nama'    => 'required',
            'nip'     => 'required',
            'gelar'   => 'required',
            'strata'  => 'required',
            'jurusan' => 'required',
            'sekolah' => 'required',
            'tahun_mulai'=> 'required',
            'tahun_selesai' => 'required'
        ]);


        $dosen = Dosen::create([
            'nama'     => $request->nama,
            'nip'     => $request->nip,
            'gelar'   => $request->gelar
        ]);

        $dosen_id = $dosen->id;
        $strata = $request->strata;
        $jurusan = $request->jurusan;
        $sekolah = $request->sekolah;
        $tahun_mulai = $request->tahun_mulai;
        $tahun_selesai = $request->tahun_selesai;

        for($i=0; $i < count($strata); $i++){
            $datasave = [
                'dosen_id' => $dosen_id,
                'strata'  => $strata[$i],
                'jurusan' => $jurusan[$i],
                'sekolah' => $sekolah[$i],
                'tahun_mulai'=> $tahun_mulai[$i],
                'tahun_selesai' => $tahun_selesai[$i]
            ];
           $save = DB::table('riwayat_pendidikan')->insert($datasave);
        }

        // return redirect()->route('dosen.index')->with(['success' => 'Data Berhasil Disimpan!']);
        if($dosen && $save){
            //redirect dengan pesan sukses
            return redirect()->route('dosen.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('dosen.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

}