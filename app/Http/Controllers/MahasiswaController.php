<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use DB;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mahasiswas = Mahasiswa::latest()->paginate(10);
        return view('mahasiswa.index', compact('mahasiswas'));
    }

    public function create()
    {
        return view('mahasiswa.create');
    }

    public function store(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            'nama'    => 'required',
            'nim'     => 'required',
            'jenis_kelamin'   => 'required',
            'ttl'  => 'required',

        ]);


        $mahasiswa = Mahasiswa::create([
            'nama'     => $request->nama,
            'nim'     => $request->nim,
            'jenis_kelamin'   => $request->jenis_kelamin,
            'ttl'   => $request->ttl,
        ]);

      
        // return redirect()->route('dosen.index')->with(['success' => 'Data Berhasil Disimpan!']);
        if($mahasiswa){
            //redirect dengan pesan sukses
            return redirect()->route('mahasiswa.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('mahasiswa.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }
}
