<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MataKuliah;


class MataKuliahController extends Controller
{
     public function index()
    {
        $matakuliahs = MataKuliah::latest()->paginate(10);
        return view('matakuliah.index', compact('matakuliahs'));
    }

    public function create()
    {
        return view('matakuliah.create');
    }

    public function store(Request $request)
    {

        // dd($request->all());
        $this->validate($request, [
            'nama'    => 'required',
            'sks'     => 'required'

        ]);


        $matakuliah = MataKuliah::create([
            'nama'     => $request->nama,
            'sks'     => $request->sks
        ]);

      
        // return redirect()->route('dosen.index')->with(['success' => 'Data Berhasil Disimpan!']);
        if($matakuliah){
            //redirect dengan pesan sukses
            return redirect()->route('matakuliah.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('matakuliah.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }
}
