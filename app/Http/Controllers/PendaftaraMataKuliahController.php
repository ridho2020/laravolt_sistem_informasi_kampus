<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Models\MataKuliah;
use App\Models\PendaftaranMatkul;

class PendaftaraMataKuliahController extends Controller
{

     public function index()
    {
        $pendaftaranmatakuliahs = PendaftaranMatkul::with('mahasiswa', 'mata_kuliah')->latest()->paginate(10);
        // dd($pendaftaranmatakuliahs);
        return view('pendaftaranmatakuliah.index', compact('pendaftaranmatakuliahs'));
    }

    
    public function create()
    {
        $mahasiswas = Mahasiswa::get();
        $matakuliahs = MataKuliah::get();
        return view('pendaftaranmatakuliah.create', compact('mahasiswas', 'matakuliahs'));
    }

     public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'mahasiswa_id' => 'required',
            'mata_kuliah_id' => 'required'
        ]);

    
        $pendaftaranmatakuliah = PendaftaranMatkul::create([
            'mahasiswa_id' => $request->mahasiswa_id,
            'mata_kuliah_id' => $request->mata_kuliah_id
        ]);

        if($pendaftaranmatakuliah){
            //redirect dengan pesan sukses
            return redirect()->route('pendaftaranmatakuliah.index')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('pendaftaranmatakuliah.index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }


}
