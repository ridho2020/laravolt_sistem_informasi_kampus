<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;
    protected $table = 'mahasiswa';
    protected $primaryKey = 'id';
    protected $fillable = ['nama','nim','jenis_kelamin','ttl'];
    public $timestamps = false;

    public function pendaftaran_matkul() {
        return $this->hasMany(PendaftaranMatkul::class);
    }

}
