<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PendaftaranMatkul extends Model
{
    use HasFactory;

    protected $table = 'pendaftaran_matkul';
    protected $fillable = ['mahasiswa_id', 'mata_kuliah_id'];
    public $timestamps = false;

    public function mahasiswa() {
        return $this->belongsTo(Mahasiswa::class);
    }
    
    public function mata_kuliah() {
        return $this->belongsTo(MataKuliah::class);
    }

}
