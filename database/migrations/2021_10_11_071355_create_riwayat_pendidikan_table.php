<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatPendidikanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('riwayat_pendidikan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('dosen_id');
            $table->integer('strata');
            $table->string('jurusan');
            $table->string('sekolah');
            $table->year('tahun_mulai');
            $table->year('tahun_selesai');
            $table->timestamps();
        });

        Schema::table('riwayat_pendidikan', function (Blueprint $table) {
            $table->foreign('dosen_id')->references('id')->on('dosen');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pendidikan');
    }
}
