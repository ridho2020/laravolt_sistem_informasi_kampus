<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDosenMataKuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('dosen_mata_kuliah', function (Blueprint $table) {
            $table->unsignedBigInteger('dosen_id');
            $table->unsignedBigInteger('mata_kuliah_id');
            $table->timestamps();
        });

        Schema::table('dosen_mata_kuliah', function (Blueprint $table) {
            $table->foreign('dosen_id')->references('id')->on('dosen');
            $table->foreign('mata_kuliah_id')->references('id')->on('mata_kuliah');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen_mata_kuliah');
    }
}
