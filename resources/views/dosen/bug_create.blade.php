<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah Data Blog - SantriKoding.com</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body style="background: lightgray">

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <form action="{{ route('dosen.store') }}" method="POST" enctype="multipart/form-data">
                        
                            @csrf

                            <div class="form-group">
                                <label class="font-weight-bold">Nama</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">NIP</label>
                                <input type="number" class="form-control @error('number') is-invalid @enderror" name="nip" value="{{ old('nip') }}" placeholder="Masukkan Nip">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">GELAR</label>
                                <input type="text" class="form-control @error('gelar') is-invalid @enderror" name="gelar" value="{{ old('gelar') }}" placeholder="Masukkan Gelar">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <!-- Strata 1 -->
                            <div class="form-group">
                                <label class="font-weight-bold">STRATA 1</label>
                                <input type="text" class="form-control @error('strata') is-invalid @enderror" name="strata[]" value="{{ old('strata') }}" placeholder="Masukkan Strata 1">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">JURUSAN</label>
                                <input type="text" class="form-control @error('jurusan') is-invalid @enderror" name="jurusan[]" value="{{ old('jurusan') }}" placeholder="Masukkan Jurusan">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">SEKOLAH</label>
                                <input type="text" class="form-control @error('sekolah') is-invalid @enderror" name="sekolah[]" value="{{ old('sekolah') }}" placeholder="Masukkan Sekolah">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">TAHUN MULAI</label>
                                <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" name="tahun_mulai[]" value="{{ old('tahun_mulai') }}" placeholder="Masukkan Tahun Mulai">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">TAHUN SELASAI</label>
                                <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" name="tahun_selesai[]" value="{{ old('tahun_selesai') }}" placeholder="Masukkan Tahun Selesai">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!-- End Strata 1 -->

                                    <!-- Strata 1 -->
                            <div class="form-group">
                                <label class="font-weight-bold">STRATA 2</label>
                                <input type="text" class="form-control @error('strata') is-invalid @enderror" name="strata[]" value="{{ old('strata') }}" placeholder="Masukkan Strata 1">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">JURUSAN</label>
                                <input type="text" class="form-control @error('jurusan') is-invalid @enderror" name="jurusan[]" value="{{ old('jurusan') }}" placeholder="Masukkan Jurusan">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">SEKOLAH</label>
                                <input type="text" class="form-control @error('sekolah') is-invalid @enderror" name="sekolah[]" value="{{ old('sekolah') }}" placeholder="Masukkan Sekolah">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">TAHUN MULAI</label>
                                <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" name="tahun_mulai[]" value="{{ old('tahun_mulai') }}" placeholder="Masukkan Tahun Mulai">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">TAHUN SELASAI</label>
                                <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" name="tahun_selesai[]" value="{{ old('tahun_selesai') }}" placeholder="Masukkan Tahun Selesai">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!-- End Strata 1 -->

                                    <!-- Strata 1 -->
                            <div class="form-group">
                                <label class="font-weight-bold">STRATA 3</label>
                                <input type="text" class="form-control @error('strata') is-invalid @enderror" name="strata[]" value="{{ old('strata') }}" placeholder="Masukkan Strata 1">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">JURUSAN</label>
                                <input type="text" class="form-control @error('jurusan') is-invalid @enderror" name="jurusan[]" value="{{ old('jurusan') }}" placeholder="Masukkan Jurusan">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">SEKOLAH</label>
                                <input type="text" class="form-control @error('sekolah') is-invalid @enderror" name="sekolah[]" value="{{ old('sekolah') }}" placeholder="Masukkan Sekolah">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">TAHUN MULAI</label>
                                <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" name="tahun_mulai[]" value="{{ old('tahun_mulai') }}" placeholder="Masukkan Tahun Mulai">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                              <div class="form-group">
                                <label class="font-weight-bold">TAHUN SELASAI</label>
                                <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" name="tahun_selesai[]" value="{{ old('tahun_selesai') }}" placeholder="Masukkan Tahun Selesai">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!-- End Strata 1 -->

                          

                            <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
                            <button type="reset" class="btn btn-md btn-warning">RESET</button>

                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
