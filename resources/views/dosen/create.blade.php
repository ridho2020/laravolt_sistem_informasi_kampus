<!DOCTYPE html>
<html>
<head>
    <title>Add/remove multiple input fields dynamically with Jquery Laravel 5.8</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
   
<div class="container">
    <h2 align="center">Masukan Data Dosen</h2> 
   
    <form action="{{ route('dosen.store') }}" method="POST">
        @csrf
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
   
        @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        
                            <div class="form-group">
                                <label class="font-weight-bold">Nama</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">NIP</label>
                                <input type="number" class="form-control @error('number') is-invalid @enderror" name="nip" value="{{ old('nip') }}" placeholder="Masukkan Nip">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label class="font-weight-bold">GELAR</label>
                                <input type="text" class="form-control @error('gelar') is-invalid @enderror" name="gelar" value="{{ old('gelar') }}" placeholder="Masukkan Gelar">
                            
                                <!-- error message untuk title -->
                                @error('title')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
        <table class="table table-bordered" id="dynamicTable">  
            <tr>
                <th>Strata</th>
                <th>Jurusan</th>
                <th>Sekolah</th>
                <th>Tahun Mulai</th>
                <th>Tahun Selesai</th>
                <th>Action</th>
            </tr>
            <tr>  
                <td><input type="number" name="strata[]" placeholder="Masukan Tingkat Strata" class="form-control" /></td>  
                <td><input type="text" name="jurusan[]" placeholder="Masukan Jurusan" class="form-control" /></td>  
                <td><input type="text" name="sekolah[]" placeholder="Masukan Nama Sekolah" class="form-control" /></td>
                 <td><input type="text" name="tahun_mulai[]" placeholder="Tahun Mulai" class="form-control" /></td>
                  <td><input type="text" name="tahun_selesai[]" placeholder="Tahun Selesai" class="form-control" /></td>
                  
                <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
            </tr>  
        </table> 
         <button type="submit" class="btn btn-success">Save</button>
    </form>
</div>
   
<script type="text/javascript">
   
    var i = 0;
       
    $("#add").click(function(){
   
        ++i;
   
        $("#dynamicTable").append('<tr><td><input type="number" name="strata[]" placeholder="Masukan Tingkat Strata" class="form-control" /></td><td><input type="text" name="jurusan[]" placeholder="Masukan Jurusan" class="form-control" /></td><td><input type="text" name="sekolah[]" placeholder="Masukan Nama Sekolah" class="form-control" /></td><td><input type="text" name="tahun_mulai[]" placeholder="Tahun Mulai" class="form-control" /></td><td><input type="text" name="tahun_selesai[]" placeholder="Tahun Selesai" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
    });
   
    $(document).on('click', '.remove-tr', function(){  
         $(this).parents('tr').remove();
    });  
   
</script>
  
</body>
</html>