<!DOCTYPE html>
<html>
<head>
	<title>Dosen</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('dosen') }}">View All Dosen</a></li>
	</ul>
</nav>

<h1>Data Lengkap Dosen</h1>

	<div class="jumbotron text-center">
		<p>Nama : {{$dosens->nama}}</p> 
        <p>Nama : {{$dosens->nip}}</p> 
        <p>Nama : {{$dosens->gelar}}</p> 
        @foreach($dosens->riwayatPendidikan as $dosen)
		<p> Strata : {{ $dosen->strata }}</p>
        <p> Jurusan : {{ $dosen->jurusan }}</p>
        <p> Sekolah : {{ $dosen->sekolah }}</p>
        <p> Sekolah : {{ $dosen->tahun_mulai }}</p>
        <p> Sekolah : {{ $dosen->tahun_selesai }}</p>
        <br>
        @endforeach
	</div>

</div>
</body>
</html>