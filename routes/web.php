<?php

use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Home;

Route::get('/', Home::class)->name('home');
Route::get('/dashboard', Dashboard::class)->name('dashboard')->middleware('auth');


Route::resource('dosen', '\App\Http\Controllers\DosenController');
Route::resource('mahasiswa', '\App\Http\Controllers\MahasiswaController');
Route::resource('matakuliah', '\App\Http\Controllers\MataKuliahController');
Route::resource('matakuliah', '\App\Http\Controllers\MataKuliahController');
Route::resource('pendaftaranmatakuliah', '\App\Http\Controllers\PendaftaraMataKuliahController');